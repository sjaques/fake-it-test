#pragma once

#include <QString>
#include <QStringList>
#include <QSharedPointer>
#include <QDebug>
class ErrorValuePrivate;


class ErrorValue
{
    Q_DECLARE_PRIVATE(ErrorValue)

public:
    ErrorValue() Q_DECL_NOTHROW;
    ErrorValue(const char *file, int line, const char *function, const char *identifier) Q_DECL_NOTHROW;
    ErrorValue(const QString &file, int line, const QString &function, const QString &identifier) Q_DECL_NOTHROW;
    ErrorValue(const ErrorValue &other) Q_DECL_NOTHROW;

    ErrorValue& operator=(const ErrorValue &other) Q_DECL_NOTHROW;

    QString identifier() const Q_DECL_NOTHROW;
    QStringList stack() const Q_DECL_NOTHROW;

    QString file() const Q_DECL_NOTHROW;
    int line() const Q_DECL_NOTHROW;
    QString function() const Q_DECL_NOTHROW;
    QString description() const Q_DECL_NOTHROW;

    ErrorValue operator+(const ErrorValue &other) Q_DECL_NOTHROW;

    QSharedPointer<ErrorValue> child() const Q_DECL_NOTHROW;

    ErrorValue& operator<<(QChar c) Q_DECL_NOTHROW;
    ErrorValue& operator<<(char c) Q_DECL_NOTHROW;
    ErrorValue& operator<<(signed short number) Q_DECL_NOTHROW;
    ErrorValue& operator<<(unsigned short number) Q_DECL_NOTHROW;
    ErrorValue& operator<<(signed int number) Q_DECL_NOTHROW;
    ErrorValue& operator<<(unsigned int number) Q_DECL_NOTHROW;
    ErrorValue& operator<<(signed long number) Q_DECL_NOTHROW;
    ErrorValue& operator<<(unsigned long number) Q_DECL_NOTHROW;
    ErrorValue& operator<<(qlonglong number) Q_DECL_NOTHROW;
    ErrorValue& operator<<(qulonglong number) Q_DECL_NOTHROW;
    ErrorValue& operator<<(float number) Q_DECL_NOTHROW;
    ErrorValue& operator<<(double number) Q_DECL_NOTHROW;
    ErrorValue& operator<<(const char *message) Q_DECL_NOTHROW;
    ErrorValue& operator<<(const QString &message) Q_DECL_NOTHROW;
    ErrorValue& operator<<(const QStringList &list) Q_DECL_NOTHROW;
    ErrorValue& operator<<(bool flag) Q_DECL_NOTHROW;

private:
    QSharedPointer<ErrorValuePrivate> d_ptr;
};

#define errorValue(identifier) ErrorValue(QT_MESSAGELOG_FILE, QT_MESSAGELOG_LINE, QT_MESSAGELOG_FUNC, identifier)

QDebug operator<<(QDebug d, const ErrorValue &value);
