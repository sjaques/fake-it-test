#ifndef RETURNERRORTOSTRING_H
#define RETURNERRORTOSTRING_H

#include "returnerror.h"
#include <QString>

/**
 * @brief Translates an error code into corresponding string
 * @param[in] err  Error code
 * @return String representation of error.  If code is invalid, return "Invalid error code".
 */
QString getErrorString(ReturnError err) Q_DECL_NOTHROW;

#endif // RETURNERRORTOSTRING_H
