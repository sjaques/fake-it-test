#pragma once

#include <QString>
#include <QStringList>
#include <QSharedPointer>
#include "returnerror.h"
class ErrorValue;


class ErrorValuePrivate
{
public:
    ErrorValuePrivate();

    ErrorValuePrivate(const QString &file,
                      int line,
                      const QString &function,
                      const QString &identifier);

    ErrorValuePrivate(const ErrorValuePrivate &other);

    QString identifier() const;

    const QString m_file;
    const int m_line;
    const QString m_function;
    const QString m_identifier;

    QString m_description;

    QSharedPointer<ErrorValue> m_child;
};
