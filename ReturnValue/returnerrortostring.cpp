#include "returnerrortostring.h"
#include <QPair>

static const QPair<ReturnError, QString> ERROR_STRING_TABLE[] = {
    { ReturnError::SUCCESS,                 QStringLiteral("Succeeded")},
    { ReturnError::COMMAND_NOT_SUPPORTED,   QStringLiteral("Requested command is not supported")},
    { ReturnError::COMMUNICATION_FAILED,    QStringLiteral("Communication failed")},
    { ReturnError::INVALID_PARAMETER,       QStringLiteral("Invalid parameter")},
    { ReturnError::NOT_INITIALIZED,         QStringLiteral("Using an improperly initialized object")},
    { ReturnError::INTERRUPTED,             QStringLiteral("Command interrupted externally")},
    { ReturnError::INVALID_RESOURCE,        QStringLiteral("Invalid resource")},
    { ReturnError::INVALID_OBJECT,          QStringLiteral("Invalid object")},
    { ReturnError::SYSTEM_ERROR,            QStringLiteral("System error")},
    { ReturnError::INVALID_CONFIGURATION,   QStringLiteral("Invalid configuration")},
    { ReturnError::OPERATION_FAILED,        QStringLiteral("Operation failed")},
    { ReturnError::TIMEOUT,                 QStringLiteral("Operation timed out")},
    { ReturnError::NOT_ALLOWED,             QStringLiteral("Parameter value is not allowed")},
    { ReturnError::LOCKED,                  QStringLiteral("The object is currently clocked")},
};

//use Q_CONSTEXPR because MSVC (again, sigh) does not support constexpr
static Q_CONSTEXPR size_t ERROR_STRING_TABLE_SIZE =
        sizeof(ERROR_STRING_TABLE) / sizeof(ERROR_STRING_TABLE[0]);

QString getErrorString(ReturnError err) Q_DECL_NOTHROW
{
    static_assert(std::is_nothrow_copy_constructible<QString>::value,
                  "QString is not no throw copy constructible. Cannot guarantee noexcept.");
    static_assert(std::is_nothrow_move_constructible<QString>::value,
                  "QString is not no throw move constructible. Cannot guarantee noexcept.");

    //You will most likely hit this compile-time assert if you added an enum value
    //to ReturnError, but did not add a corresponding error string to the table above.
    static_assert(ERROR_STRING_TABLE_SIZE == static_cast<int>(ReturnError::END_OF_LIST),
                  "Display error string table has wrong number of strings");
    static_assert(static_cast<int>(ReturnError::SUCCESS) == 0,
                  "ReturnError::SUCCESS must be equal to 0.");

    // Searching through table rather than directly indexing to avoid mismatch between
    // order of enums and their corresponding string values in the table.  If efficiency
    // becomes a problem, can switch to indexing into table, but will need to be very
    // careful about ensuring that the order match.
    // KUPA: I have tested with a switch statement, but that is much slower than this implementation.
    // KUPA: A loop of 1 million times fetching all error code strings from SUCCESS to and including
    // KUPA: END_OF_LIST takes 18 msecs on my MacBook Pro (compared with the switch case where it
    // KUPA: was 185msecs; but this was a debug build and it is known that switch cases can be
    // KUPA: very seriously optimized).
    // KUPA: Doing 6 million lookups in 18msecs is more than fast enough.
    if (Q_LIKELY(err < ReturnError::END_OF_LIST)) {
        for (size_t i = 0u; i < ERROR_STRING_TABLE_SIZE; ++i)
        {
            if (ERROR_STRING_TABLE[i].first == err)
            {
                return ERROR_STRING_TABLE[i].second;
            }
        }
    }

    return QStringLiteral("Invalid error code");
}
