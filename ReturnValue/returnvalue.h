#pragma once

#include "returnvaluebase.h"
#include "returnerror.h"
#include <QDebug>

//FIXME: fix MSVC compiler error
#ifndef Q_CC_MSVC
#if __cpp_lib_is_swappable
    //std::is_nothrow_swappable is implemented, so we don't
    //need to  implement our own is_nothrow_swappable trait.
#else
    #include <type_traits>  //for enable_if
    #include <utility>      //for declval & swap
    namespace std {
        template <typename T, typename = void>
        struct is_nothrow_swappable : public std::false_type {
        };

        template <typename T>
        struct is_nothrow_swappable<T, typename std::enable_if<noexcept(
            std::swap(std::declval<T&>(),std::declval<T&>()))>::type> : public std::true_type {
        };
    } //end namespace std
#endif // __cpp_lib_is_swappable
#endif // Q_CC_MSVC

/**
 * @brief ReturnValue is a helper template class that makes it possible to return both a
 * regular value and an error from a method or function call.
 *
 * If a method needs to fetch or store values over an unreliable messaging path (like over o
 * network), the classic function signature `returnValue getSomeValue()` cannot return
 * an eventual communication error.
 * Unlike in Go or Python, C++ does not allow to return multiple values from a function call.
 * The classic approach is to always return an error value and supply the requested output value
 * as a pointer to the method call.
 * E.g. `Error getSomething(int *value)`.
 * While perfectly legal, this leads to heavy C-like code, both in the implementation of the method
 * and in calling it.
 * @code
 * Error getSomething(int *value) {
 *   assert(value); //make sure value is a valid pointer
 *   if (everythingWentOk) {
 *     *value = theValue;
 *     return OK;
 *   } else {
 *     *value = 0;  //set the value to some default
 *     return SOME_SERIOUS_ERROR;
 *   }
 * }
 *
 * //When calling the code
 * int value;  //declare variable to receive the result
 * Error e = getSomething(&value);
 * if (e == SUCCESS) {
 *   //do something with value
 * } else {
 *   //do something with error condition
 * }
 * //from here on I can still use value as if nothing happened
 * @endcode
 * @note In the implementation above it is perfectly possible to use the return value even if
 * there was an error.
 *
 * By using ReturnValue the implementation becomes much simpler and the return value is protected
 * by accidental use in case of an error (although this is only in debug builds).
 * @code
 * ReturnValue<int> getSomething() {
 *   if (everythingWentOk) {
 *     return theValue;
 *   } else {
 *     return SOME_SERIOUS_ERROR;
 *   }
 * }
 *
 * //When calling the code
 * ReturnValue<int> value = getSomething();
 * if (value.isValid()) {
 *   //do something with value.value()
 * } else {
 *   //do something with value.error();
 * }
 * //if value.value() is called on an invalid return value (!ReturnValue::isValid()) an assertion
 * //will be thrown at runtime (for debug builds).
 * @endcode
 * @note The implementation of the getSomething() method is much more straightforward.
 * Usage of the return value is protected by an assertion during debug builds, leading to much
 * better error detection during development.
 *
 * To have more consistent code, ReturnValue can be used with void return types as well.
 * ReturnValue<void> does not have a value() method, but has an error() method.
 *
 * @paragraph Supported types
 * The ReturnValue class can hold return types that are:
 * - non-throwable default constructible
 * - non-throwable move constructible
 * - non-throwable move assignable
 * - copy assignable (i.e. must have an assignment operator)
 * - copy constructible
 * - non-throwable swappable
 *
 * @tparam Type the type of the return value. The Type must be a supported type (see above).
 */
template <typename Type>
class ReturnValue Q_DECL_FINAL: public ReturnValueBase
{
public:
    inline ReturnValue();
    /**
     * @brief Constructs a new ReturnValue initialized with the given value. The error will be
     * set to ReturnError::SUCCESS. ReturnValue::isValid() will return true.
     */
    inline ReturnValue(const Type &value);
    /**
     * @brief Constructs a new ReturnValue initialized with the given error. The value will be
     * set to the default constructed value of the type. ReturnValue::isValid() will return false.
     */
    inline ReturnValue(const ReturnError &error, const QString &detail = QString()) Q_DECL_NOTHROW;
    inline ReturnValue(const ReturnValueBase &other) Q_DECL_NOTHROW;
    /**
      * @brief Use a default constructed destructor
      */
    ~ReturnValue();

    /**
     * @brief Creates a new ReturnValue as a copy of the other ReturnValue.
     */
    inline ReturnValue(const ReturnValue<Type> &other) Q_DECL_NOTHROW;
    /**
     * @brief Creates a new ReturnValue by moving the other ReturnValue into this one.
     */
    inline ReturnValue(ReturnValue<Type> &&other) Q_DECL_NOTHROW;


    //operator = cannot be a constexpr in C++11 or earlier, because it contains an if-clause
    /**
     * @brief Copies the other ReturnValue into this one. Returns a reference to itself.
     */
    inline ReturnValue<Type> &operator =(const ReturnValue<Type> &other) Q_DECL_NOTHROW;
    //cannot be a constexpr in C++11 or earlier, because swap is not supported in constexpr before C++14
    /**
     * @brief Copies the other ReturnValue into this one, by moving the values from other to this
     * object. Returns a reference to itself.
     */
    inline ReturnValue<Type> &operator =(ReturnValue<Type> &&other) Q_DECL_NOTHROW;

    /**
     * @brief Swaps the other object by this one.
     */
    inline void swap(ReturnValue<Type> &other) Q_DECL_NOTHROW;

    //cannot be a constexpr in C++11 or earlier, because it contains an if-clause (Q_ASSERT)
    /**
     * @brief Returns the stored value.
     * @note In debug builds calling this method on an invalid object will result in an assertion
     * failure. This helps in avoiding programming errors by detecting when return values are used
     * when they are not supposed to be used.
     */
    Type value() { Q_ASSERT(isValid()); return m_value; }
    //cannot be a constexpr in C++11 or earlier, because it contains an if-clause (Q_ASSERT)
    /**
     * @brief Returns a const reference to the stored value.
     * The same remarks hold as the non-const value() method.
     */
    const Type &value() const Q_DECL_NOTHROW { Q_ASSERT(isValid()); return m_value; }

    operator Type() Q_DECL_NOTHROW { Q_ASSERT(isValid()); return m_value; }
    operator const Type&() const Q_DECL_NOTHROW { Q_ASSERT(isValid()); return m_value; }

private:
    Type m_value;
};

/**
 * @brief ReturnValue<void> is a specialized template that can handle void return values.
 * It has no value() method and can only take a ReturnError during construction.
 * For the rest, it behaves exactly the same as the general ReturnValue<Type> template.
 *
 * This specialized template was created to make void returning methods consistent with
 * non-void returning methods. In both cases, a ReturnValue is returned.
 */
template<>
class ReturnValue<void> Q_DECL_FINAL: public ReturnValueBase
{
public:
    inline ReturnValue() Q_DECL_NOTHROW;
    inline ReturnValue(const ReturnError &error, const QString &detail = QString()) Q_DECL_NOTHROW;
    inline ReturnValue(const ReturnValueBase &other) Q_DECL_NOTHROW;
    inline ReturnValue(const ReturnValue &other) Q_DECL_NOTHROW;
    ~ReturnValue();

    //operator = cannot be a constexpr in C++11 or earlier, because it contains an if-clause
    /**
     * @brief Copies the other ReturnValue into this one. Returns a reference to itself.
     */
    inline ReturnValue<void> &operator =(const ReturnValue<void> &other) Q_DECL_NOTHROW;
    //cannot be a constexpr in C++11 or earlier, because swap is not supported in constexpr before C++14
    /**
     * @brief Copies the other ReturnValue into this one, by moving the values from other to this
     * object. Returns a reference to itself.
     */
    inline ReturnValue<void> &operator =(ReturnValue<void> &&other) Q_DECL_NOTHROW;

    /**
     * @brief Swaps the other object by this one.
     */
    inline void swap(ReturnValue<void> &other) Q_DECL_NOTHROW;
};

/**
 * @brief ReturnValue<QSharedPointer<T>> is a specialized template that can handle QSharedPointer return values.
 * Due to a bug in QSharedPointer, the requirement for a non throwable swappable type is dropped.
 */
template <typename Type>
class ReturnValue<QSharedPointer<Type>> Q_DECL_FINAL: public ReturnValueBase
{
public:
    inline ReturnValue();
    /**
     * @brief Constructs a new ReturnValue initialized with the given value. The error will be
     * set to ReturnError::SUCCESS. ReturnValue::isValid() will return true.
     */
    inline ReturnValue(const QSharedPointer<Type> &value);
    /**
     * @brief Constructs a new ReturnValue initialized with the given error. The value will be
     * set to the default constructed value of the type. ReturnValue::isValid() will return false.
     */
    inline ReturnValue(const ReturnError &error, const QString &detail = QString()) Q_DECL_NOTHROW;
    inline ReturnValue(const ReturnValueBase &other) Q_DECL_NOTHROW;
    /**
      * @brief Use a default constructed destructor
      */
    ~ReturnValue();

    /**
     * @brief Creates a new ReturnValue as a copy of the other ReturnValue.
     */
    inline ReturnValue(const ReturnValue<QSharedPointer<Type>> &other) Q_DECL_NOTHROW;
    /**
     * @brief Creates a new ReturnValue by moving the other ReturnValue into this one.
     */
    inline ReturnValue(ReturnValue<QSharedPointer<Type>> &&other) Q_DECL_NOTHROW;


    //operator = cannot be a constexpr in C++11 or earlier, because it contains an if-clause
    /**
     * @brief Copies the other ReturnValue into this one. Returns a reference to itself.
     */
    inline ReturnValue<QSharedPointer<Type>> &operator =(const ReturnValue<QSharedPointer<Type>> &other) Q_DECL_NOTHROW;
    //cannot be a constexpr in C++11 or earlier, because swap is not supported in constexpr before C++14
    /**
     * @brief Copies the other ReturnValue into this one, by moving the values from other to this
     * object. Returns a reference to itself.
     */
    inline ReturnValue<QSharedPointer<Type>> &operator =(ReturnValue<QSharedPointer<Type>> &&other) Q_DECL_NOTHROW;

    /**
     * @brief Swaps the other object by this one.
     */
    inline void swap(ReturnValue<QSharedPointer<Type>> &other) Q_DECL_NOTHROW;

    //cannot be a constexpr in C++11 or earlier, because it contains an if-clause (Q_ASSERT)
    /**
     * @brief Returns the stored value.
     * @note In debug builds calling this method on an invalid object will result in an assertion
     * failure. This helps in avoiding programming errors by detecting when return values are used
     * when they are not supposed to be used.
     */
    QSharedPointer<Type> value() { Q_ASSERT(isValid()); return m_value; }
    //cannot be a constexpr in C++11 or earlier, because it contains an if-clause (Q_ASSERT)
    /**
     * @brief Returns a const reference to the stored value.
     * The same remarks hold as the non-const value() method.
     */
    const QSharedPointer<Type> &value() const Q_DECL_NOTHROW { Q_ASSERT(isValid()); return m_value; }


private:
    QSharedPointer<Type> m_value;
};

template<typename Type>
inline ReturnValue<Type>::ReturnValue():
    ReturnValueBase(),
    m_value()
{
}

template<typename Type>
inline ReturnValue<Type>::ReturnValue(const Type &value):
    ReturnValueBase(),
    m_value(value)
{
    static_assert(std::is_nothrow_default_constructible<Type>::value,
                  "Type has no default non throwable constructor");
    static_assert(std::is_copy_constructible<Type>::value,
                  "Type has no copy constructor");
    static_assert(std::is_nothrow_move_constructible<Type>::value,
                  "Type is not non throwable move constructible");
    static_assert(std::is_nothrow_move_assignable<Type>::value,
                  "Type is not non throwable move-assignable.");
    static_assert(std::is_copy_assignable<Type>::value,
                  "Type is not copy-assignable.");
//FIXME: fix MSVC compiler error in is_nothrow_swappable
#ifndef Q_CC_MSVC
    static_assert(std::is_nothrow_swappable<Type>::value,
                  "Type is not non throwable swappable");
#endif
}

template<typename Type>
inline ReturnValue<Type>::ReturnValue(const ReturnError &error, const QString &detail) Q_DECL_NOTHROW:
    ReturnValueBase(error, detail),
    m_value()
{
    static_assert(std::is_nothrow_default_constructible<Type>::value,
                  "Type has no default non throwable constructor");
    static_assert(std::is_copy_constructible<Type>::value,
                  "Type has no copy constructor");
    static_assert(std::is_nothrow_move_constructible<Type>::value,
                  "Type is not non throwable move constructible");
    static_assert(std::is_nothrow_move_assignable<Type>::value,
                  "Type is not non throwable move-assignable.");
    static_assert(std::is_copy_assignable<Type>::value,
                  "Type is not copy-assignable.");
    //FIXME: fix MSVC compiler error in is_nothrow_swappable
#ifndef Q_CC_MSVC
    static_assert(std::is_nothrow_swappable<Type>::value,
                  "Type is not non throwable swappable");
#endif
}

template<typename Type>
inline ReturnValue<Type>::ReturnValue(const ReturnValue<Type> &other) Q_DECL_NOTHROW:
    ReturnValueBase(other),
    m_value(other.m_value)
{
}

template<typename Type>
inline ReturnValue<Type>::ReturnValue(ReturnValue<Type> &&other) Q_DECL_NOTHROW :
    ReturnValueBase(other),
    m_value(std::move(other.m_value))
{
}

template<typename Type>
inline ReturnValue<Type> &ReturnValue<Type>::operator =(const ReturnValue<Type> &other) Q_DECL_NOTHROW
{
    if (Q_LIKELY(this != &other))
    {
        ReturnValueBase::operator=(other);
        m_value = other.m_value;
    }
    return *this;
}

template<typename Type>
inline ReturnValue<Type> &ReturnValue<Type>::operator =(ReturnValue<Type> &&other) Q_DECL_NOTHROW
{
    ReturnValueBase::operator =(other);
    qSwap(m_value, other.m_value);
    return *this;
}

template<typename Type>
inline void ReturnValue<Type>::swap(ReturnValue<Type> &other) Q_DECL_NOTHROW
{
    ReturnValueBase::swap(other);
    qSwap(m_value, other.m_value);
}

inline ReturnValue<void>::ReturnValue() Q_DECL_NOTHROW:
    ReturnValueBase()
{
}

inline ReturnValue<void>::ReturnValue(const ReturnError &error, const QString &detail) Q_DECL_NOTHROW:
    ReturnValueBase(error, detail)
{
}

inline ReturnValue<void>::ReturnValue(const ReturnValueBase &other) Q_DECL_NOTHROW:
    ReturnValueBase(other)
{
}

inline ReturnValue<void>::ReturnValue(const ReturnValue &other) Q_DECL_NOTHROW:
    ReturnValueBase(other)
{
}

inline ReturnValue<void>::~ReturnValue()
{
}

inline ReturnValue<void> &ReturnValue<void>::operator =(const ReturnValue<void> &other) Q_DECL_NOTHROW
{
    if (Q_LIKELY(this != &other))
    {
        ReturnValueBase::operator=(other);
    }
    return *this;
}

inline ReturnValue<void> &ReturnValue<void>::operator =(ReturnValue<void> &&other) Q_DECL_NOTHROW
{
    ReturnValueBase::operator =(other);
    return *this;
}

inline void ReturnValue<void>::swap(ReturnValue<void> &other) Q_DECL_NOTHROW
{
    ReturnValueBase::swap(other);
}

template<typename Type>
ReturnValue<Type>::ReturnValue(const ReturnValueBase &other) Q_DECL_NOTHROW:
    ReturnValueBase(other),
    m_value()
{
}

template<typename Type>
ReturnValue<Type>::~ReturnValue()
{
}

template<typename Type>
QDebug operator<< (QDebug d, const ReturnValue<Type> &value)
{
    d.noquote() << value.errorString();
    return d;
}

template<typename Type>
inline ReturnValue<QSharedPointer<Type>>::ReturnValue():
    ReturnValueBase(),
    m_value()
{
}

template<typename Type>
inline ReturnValue<QSharedPointer<Type>>::ReturnValue(const QSharedPointer<Type> &value):
    ReturnValueBase(),
    m_value(value)
{
}

template<typename Type>
inline ReturnValue<QSharedPointer<Type>>::ReturnValue(const ReturnError &error, const QString &detail) Q_DECL_NOTHROW:
    ReturnValueBase(error, detail),
    m_value()
{
}

template<typename Type>
inline ReturnValue<QSharedPointer<Type>>::ReturnValue(const ReturnValue<QSharedPointer<Type>> &other) Q_DECL_NOTHROW:
    ReturnValueBase(other),
    m_value(other.m_value)
{
}

template<typename Type>
inline ReturnValue<QSharedPointer<Type>>::ReturnValue(ReturnValue<QSharedPointer<Type>> &&other) Q_DECL_NOTHROW :
    ReturnValueBase(other),
    m_value(std::move(other.m_value))
{
}

template<typename Type>
ReturnValue<QSharedPointer<Type>>::ReturnValue(const ReturnValueBase &other) Q_DECL_NOTHROW:
    ReturnValueBase(other),
    m_value()
{
}

template<typename Type>
inline ReturnValue<QSharedPointer<Type>>::~ReturnValue()
{
}

template<typename Type>
inline ReturnValue<QSharedPointer<Type>> &ReturnValue<QSharedPointer<Type>>::operator =(const ReturnValue<QSharedPointer<Type>> &other) Q_DECL_NOTHROW
{
    if (Q_LIKELY(this != &other))
    {
        ReturnValueBase::operator=(other);
        m_value = other.m_value;
    }
    return *this;
}

template<typename Type>
inline ReturnValue<QSharedPointer<Type>> &ReturnValue<QSharedPointer<Type>>::operator =(ReturnValue<QSharedPointer<Type>> &&other) Q_DECL_NOTHROW
{
    ReturnValueBase::operator =(other);
    qSwap(m_value, other.m_value);
    return *this;
}

template<typename Type>
inline void ReturnValue<QSharedPointer<Type>>::swap(ReturnValue<QSharedPointer<Type>> &other) Q_DECL_NOTHROW
{
    ReturnValueBase::swap(other);
    qSwap(m_value, other.m_value);
}
