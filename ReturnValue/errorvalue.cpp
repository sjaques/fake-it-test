#include "errorvalue.h"
#include "errorvalue_p.h"
#include <QChar>

ErrorValue::ErrorValue() Q_DECL_NOTHROW:
    d_ptr(new ErrorValuePrivate())
{

}

ErrorValue::ErrorValue(const char *file,
                       int line,
                       const char *function,
                       const char *identifier) Q_DECL_NOTHROW:
    d_ptr(new ErrorValuePrivate(QString::fromLatin1(file),
                                line,
                                QString::fromLatin1(function),
                                QString::fromLatin1(identifier)))
{

}

ErrorValue::ErrorValue(const QString &file, int line, const QString &function, const QString &identifier) Q_DECL_NOTHROW:
    d_ptr(new ErrorValuePrivate(file,
                                line,
                                function,
                                identifier))
{

}

ErrorValue::ErrorValue(const ErrorValue &other) Q_DECL_NOTHROW:
    d_ptr(new ErrorValuePrivate(*other.d_ptr))
{

}

ErrorValue &ErrorValue::operator=(const ErrorValue &other) Q_DECL_NOTHROW
{
    if (this != &other)
    {
        d_ptr = other.d_ptr;
    }
    return *this;
}

QString ErrorValue::identifier() const Q_DECL_NOTHROW
{
    Q_D(const ErrorValue);
    return d->identifier();
}

QStringList ErrorValue::stack() const Q_DECL_NOTHROW
{
    QStringList result;

    Q_D(const ErrorValue);
    if (!d->m_child.isNull())
    {
        result << d->m_child->stack();
    }

    QString l = QString(QStringLiteral("%1 (%2:%3) %4"))
            .arg(function())
            .arg(file())
            .arg(line())
            .arg(description());
    result << l;

    return result;
}

QString ErrorValue::file() const Q_DECL_NOTHROW
{
    Q_D(const ErrorValue);
    return d->m_file;
}

int ErrorValue::line() const Q_DECL_NOTHROW
{
    Q_D(const ErrorValue);
    return d->m_line;
}

QString ErrorValue::function() const Q_DECL_NOTHROW
{
    Q_D(const ErrorValue);
    return d->m_function;
}

QString ErrorValue::description() const Q_DECL_NOTHROW
{
    Q_D(const ErrorValue);
    return d->m_description;
}

ErrorValue ErrorValue::operator+(const ErrorValue &other) Q_DECL_NOTHROW
{
    ErrorValue result(*this);

    QSharedPointer<ErrorValue> otherPtr = QSharedPointer<ErrorValue>::create(other);
    QSharedPointer<ErrorValue> parent = result.child();
    if (!parent.isNull())
    {
        while (!parent->child().isNull())
        {
            parent = parent->child();
        }
        parent->d_ptr->m_child = otherPtr;
    }
    else
    {
        result.d_ptr->m_child = otherPtr;
    }

    return result;
}

QSharedPointer<ErrorValue> ErrorValue::child() const Q_DECL_NOTHROW
{
    Q_D(const ErrorValue);
    return d->m_child;
}

ErrorValue &ErrorValue::operator<<(QChar c) Q_DECL_NOTHROW
{
    return operator<<(QString(c));
}

ErrorValue &ErrorValue::operator<<(char c) Q_DECL_NOTHROW
{
    return operator<<(QChar::fromLatin1(c));
}

ErrorValue &ErrorValue::operator<<(signed short number) Q_DECL_NOTHROW
{
    return operator<<(QString::number(number));
}

ErrorValue &ErrorValue::operator<<(unsigned short number) Q_DECL_NOTHROW
{
    return operator<<(QString::number(static_cast<uint>(number)));
}

ErrorValue &ErrorValue::operator<<(signed int number) Q_DECL_NOTHROW
{
    return operator<<(QString::number(number));
}

ErrorValue &ErrorValue::operator<<(unsigned int number) Q_DECL_NOTHROW
{
    return operator<<(QString::number(number));
}

ErrorValue &ErrorValue::operator<<(signed long number) Q_DECL_NOTHROW
{
    return operator<<(QString::number(number));
}

ErrorValue &ErrorValue::operator<<(unsigned long number) Q_DECL_NOTHROW
{
    return operator<<(QString::number(number));
}

ErrorValue &ErrorValue::operator<<(qlonglong number) Q_DECL_NOTHROW
{
    return operator<<(QString::number(number));
}

ErrorValue &ErrorValue::operator<<(qulonglong number) Q_DECL_NOTHROW
{
    return operator<<(QString::number(number));
}

ErrorValue &ErrorValue::operator<<(float number) Q_DECL_NOTHROW
{
    return operator<<(QString::number(number));
}

ErrorValue &ErrorValue::operator<<(double number) Q_DECL_NOTHROW
{
    return operator<<(QString::number(number));
}

ErrorValue &ErrorValue::operator<<(const char *message) Q_DECL_NOTHROW
{
    return operator<<(QString::fromLatin1(message));
}

ErrorValue &ErrorValue::operator<<(const QString &message) Q_DECL_NOTHROW
{
    Q_D(ErrorValue);
    if (!d->m_description.isEmpty())
        d->m_description.append(QStringLiteral(" "));

    d->m_description += message;

    return *this;
}

ErrorValue &ErrorValue::operator<<(const QStringList &list) Q_DECL_NOTHROW
{
    return operator<<(list.join(QStringLiteral(", ")));
}

ErrorValue &ErrorValue::operator<<(bool flag) Q_DECL_NOTHROW
{
    return operator<<(flag ? QStringLiteral("true") : QStringLiteral("false"));
}

ErrorValuePrivate::ErrorValuePrivate():
    m_file(),
    m_line(0),
    m_function(),
    m_identifier(),
    m_description(),
    m_child()
{}

ErrorValuePrivate::ErrorValuePrivate(const QString &file,
                                     int line,
                                     const QString &function,
                                     const QString &identifier):
    m_file(file),
    m_line(line),
    m_function(function),
    m_identifier(identifier),
    m_description(),
    m_child()
{}

ErrorValuePrivate::ErrorValuePrivate(const ErrorValuePrivate &other):
    m_file(other.m_file),
    m_line(other.m_line),
    m_function(other.m_function),
    m_identifier(other.m_identifier),
    m_description(other.m_description),
    m_child(other.m_child)
{}

QString ErrorValuePrivate::identifier() const
{
    QStringList result;
    result << m_identifier;
    if (!m_child.isNull())
    {
        result << m_child->identifier();
    }
    return result.join(QStringLiteral("."));
}

QDebug operator<<(QDebug d, const ErrorValue &value)
{
    for (const QString &l: value.stack())
    {
        d << qPrintable(l) << endl;
    }
    return d;
}
