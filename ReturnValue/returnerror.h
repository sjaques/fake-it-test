#ifndef RETURNERROR_H
#define RETURNERROR_H

/**
* IMPORTANT: This file is shared with the hcs-plugins repository,
* so no Qt code may be put in here!
*/

#include <cstdint>

/**
 * @brief Return error codes
 *
 * This class is deprecated. Do not use in new designs.
 */
enum class ReturnError : int32_t
{
    SUCCESS = 0,           // Method succeeded
    COMMAND_NOT_SUPPORTED, // Physical display does not support the command
    COMMUNICATION_FAILED,  // Communication with the physical display failed
    INVALID_PARAMETER,     // Method was invoked with invalid parameters
    NOT_INITIALIZED,       // Method was invoked on an improperly initialized object
    INTERRUPTED,           // Method was interrupted from outside (e.g. timed wait)
    INVALID_RESOURCE,      // Invalid resource
    INVALID_OBJECT,        // Method called on invalid object
    SYSTEM_ERROR,          // Error related to operating system
    INVALID_CONFIGURATION, // Error related to invalid HW/SW configuration
    OPERATION_FAILED,      // The operation failed
    TIMEOUT,               // Operation timed out
    NOT_ALLOWED,            // Method was invoked with parameter value(s) that is/are not allowed by the policy
    LOCKED,                // The object is locked.
    // Last entry of the enum marks the end of the list; add new values above
    END_OF_LIST            // Can use this value to mark end of enum list (e.g. in a for-loop)
};

#endif // RETURNERROR_H
