#pragma once

#include "returnerror.h"
#include <QString>
#include <QStringList>


class ReturnValueBase
{
public:
    ReturnValueBase() Q_DECL_NOTHROW;
    ReturnValueBase(const ReturnError &error, const QString &detail = QString()) Q_DECL_NOTHROW;
    ReturnValueBase(const ReturnValueBase &other) Q_DECL_NOTHROW;
    ReturnValueBase(ReturnValueBase &&other) Q_DECL_NOTHROW;

    ReturnValueBase& operator=(const ReturnValueBase &other) Q_DECL_NOTHROW;
    ReturnValueBase& operator=(ReturnValueBase &&other) Q_DECL_NOTHROW;

    void swap(ReturnValueBase &other) Q_DECL_NOTHROW;

    /**
     * @brief Returns true if the return value does not contain an error (i.e. error ==
     * ReturnError::SUCCESS).
     */
    bool isValid() const Q_DECL_NOTHROW;

    /**
     * @brief Returns the stored error. The error can be ReturnError::SUCCESS, indicating that
     * there is not an error and that the stored value can be safely used.
     */
    ReturnError error() const Q_DECL_NOTHROW;
    //! @brief Returns the stored detail description
    QString detail() const Q_DECL_NOTHROW;
    //! @brief Returns a human-readable string of the error.
    QString errorString() const Q_DECL_NOTHROW;

    ReturnValueBase& operator<<(QChar c) Q_DECL_NOTHROW;
    ReturnValueBase& operator<<(char c) Q_DECL_NOTHROW;
    ReturnValueBase& operator<<(signed short number) Q_DECL_NOTHROW;
    ReturnValueBase& operator<<(unsigned short number) Q_DECL_NOTHROW;
    ReturnValueBase& operator<<(signed int number) Q_DECL_NOTHROW;
    ReturnValueBase& operator<<(unsigned int number) Q_DECL_NOTHROW;
    ReturnValueBase& operator<<(signed long number) Q_DECL_NOTHROW;
    ReturnValueBase& operator<<(unsigned long number) Q_DECL_NOTHROW;
    ReturnValueBase& operator<<(qlonglong number) Q_DECL_NOTHROW;
    ReturnValueBase& operator<<(qulonglong number) Q_DECL_NOTHROW;
    ReturnValueBase& operator<<(float number) Q_DECL_NOTHROW;
    ReturnValueBase& operator<<(double number) Q_DECL_NOTHROW;
    ReturnValueBase& operator<<(const char *message) Q_DECL_NOTHROW;
    ReturnValueBase& operator<<(const QString &message) Q_DECL_NOTHROW;
    ReturnValueBase& operator<<(const QStringList &list) Q_DECL_NOTHROW;
    ReturnValueBase& operator<<(bool flag) Q_DECL_NOTHROW;

private:
    void appendSpace();

    ReturnError m_error;
    QString m_detail;
};
