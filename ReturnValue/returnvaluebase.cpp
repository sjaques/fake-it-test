#include "returnvaluebase.h"
#include "returnerrortostring.h"

ReturnValueBase::ReturnValueBase() Q_DECL_NOTHROW:
    m_error(ReturnError::SUCCESS),
    m_detail()
{
}

ReturnValueBase::ReturnValueBase(const ReturnError &error, const QString &detail) Q_DECL_NOTHROW:
    m_error(error),
    m_detail(detail)
{
}

ReturnValueBase::ReturnValueBase(const ReturnValueBase &other) Q_DECL_NOTHROW:
    m_error(other.m_error),
    m_detail(other.m_detail)
{
}

ReturnValueBase::ReturnValueBase(ReturnValueBase &&other) Q_DECL_NOTHROW:
    m_error(std::move(other.m_error)),
    m_detail(std::move(other.m_detail))
{
}

ReturnValueBase &ReturnValueBase::operator=(const ReturnValueBase &other) Q_DECL_NOTHROW
{
    if (Q_LIKELY(this != &other))
    {
        m_error = other.m_error;
        m_detail = other.m_detail;
    }
    return *this;
}

ReturnValueBase &ReturnValueBase::operator=(ReturnValueBase &&other) Q_DECL_NOTHROW
{
    qSwap(m_error, other.m_error);
    qSwap(m_detail, other.m_detail);
    return *this;
}

void ReturnValueBase::swap(ReturnValueBase &other) Q_DECL_NOTHROW
{
    qSwap(m_error, other.m_error);
    qSwap(m_detail, other.m_detail);
}

bool ReturnValueBase::isValid() const Q_DECL_NOTHROW
{
    return m_error == ReturnError::SUCCESS;
}

ReturnError ReturnValueBase::error() const Q_DECL_NOTHROW
{
    return m_error;
}

QString ReturnValueBase::detail() const Q_DECL_NOTHROW
{
    return m_detail;
}

QString ReturnValueBase::errorString() const Q_DECL_NOTHROW
{
    if (m_detail.isEmpty())
    {
        return getErrorString(m_error);
    }
    return getErrorString(m_error) + QStringLiteral(" [") + m_detail + QStringLiteral("]");
}

ReturnValueBase &ReturnValueBase::operator<<(QChar c) Q_DECL_NOTHROW
{
    appendSpace();
    m_detail += c;
    return *this;
}

ReturnValueBase &ReturnValueBase::operator<<(char c) Q_DECL_NOTHROW
{
    return operator<<(QChar::fromLatin1(c));
}

ReturnValueBase &ReturnValueBase::operator<<(signed short number) Q_DECL_NOTHROW
{
    return operator<<(QString::number(number));
}

ReturnValueBase &ReturnValueBase::operator<<(unsigned short number) Q_DECL_NOTHROW
{
    return operator<<(QString::number(static_cast<uint>(number)));
}

ReturnValueBase &ReturnValueBase::operator<<(signed int number) Q_DECL_NOTHROW
{
    return operator<<(QString::number(number));
}

ReturnValueBase &ReturnValueBase::operator<<(unsigned int number) Q_DECL_NOTHROW
{
    return operator<<(QString::number(number));
}

ReturnValueBase &ReturnValueBase::operator<<(signed long number) Q_DECL_NOTHROW
{
    return operator<<(QString::number(number));
}

ReturnValueBase &ReturnValueBase::operator<<(unsigned long number) Q_DECL_NOTHROW
{
    return operator<<(QString::number(number));
}

ReturnValueBase &ReturnValueBase::operator<<(qlonglong number) Q_DECL_NOTHROW
{
    return operator<<(QString::number(number));
}

ReturnValueBase &ReturnValueBase::operator<<(qulonglong number) Q_DECL_NOTHROW
{
    return operator<<(QString::number(number));
}

ReturnValueBase &ReturnValueBase::operator<<(float number) Q_DECL_NOTHROW
{
    return operator<<(QString::number(number));
}

ReturnValueBase &ReturnValueBase::operator<<(double number) Q_DECL_NOTHROW
{
    return operator<<(QString::number(number));
}

ReturnValueBase &ReturnValueBase::operator<<(const char *message) Q_DECL_NOTHROW
{
    return operator<<(QString::fromLatin1(message));
}

ReturnValueBase &ReturnValueBase::operator<<(const QString &message) Q_DECL_NOTHROW
{
    appendSpace();
    m_detail += message;
    return *this;
}

ReturnValueBase &ReturnValueBase::operator<<(const QStringList &list) Q_DECL_NOTHROW
{
    appendSpace();
    m_detail += QStringLiteral("[");
    m_detail += list.join(QStringLiteral(", "));
    m_detail += QStringLiteral("]");
    return *this;
}

ReturnValueBase &ReturnValueBase::operator<<(bool flag) Q_DECL_NOTHROW
{
    return operator<<(flag ? QStringLiteral("true") : QStringLiteral("false"));
}

void ReturnValueBase::appendSpace()
{
    if (!m_detail.isEmpty())
        m_detail.append(QStringLiteral(" "));
}
