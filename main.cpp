#include <QDebug>
#include <QVector>
#include <QString>
#include <QtTest>
#include "Frameworks/fakeit.hpp"
#include "ReturnValue/returnvalue.h"

using namespace fakeit;

struct Colors
{
    quint32 red, green, blue;
};
bool operator==(const Colors& lhs, const Colors& rhs) {
    return lhs.red == rhs.red
        && lhs.green == rhs.green
        && lhs.blue == rhs.blue;
}

struct IProtocol {
    virtual int foo(int) = 0;
    virtual ~IProtocol(){};
};

struct Protocol : public IProtocol {
    int foo(int) override { qDebug() << "This is not the stub of foo"; return 0; }
    virtual int bar(int, int) { qDebug() << "This is not the stub of bar"; return 0; }
    virtual bool getColors(Colors& colors) { qDebug() << "This is not the stub of getColors"; return 0; }
    virtual ReturnValue<Colors> getColors() const { qDebug() << "This is not the stub of bar"; return {}; }
};

struct Implementor {
    Implementor(QSharedPointer<Protocol> protocol) : m_protocol(protocol) {};
    virtual int foo(int i) { return m_protocol->foo(i); }
    virtual int bar(int i, int j) { return m_protocol->bar(i, j); }
    virtual bool getColors(Colors& colors) { return m_protocol->getColors(colors); }
    QSharedPointer<Protocol> m_protocol{QSharedPointer<Protocol>::create()};
};


class SimpleTest : public QObject
{
    Q_OBJECT
public:
    SimpleTest() = default;

private Q_SLOTS:

    void testFakeItBasic()
    {
        Mock<Protocol> mock;
        // Stub a method to return a value once
        When(Method(mock,foo)).Return(1);

        // Stub multiple return values (The next two lines do exactly the same)
        When(Method(mock,foo)).Return(1,2,3);
        When(Method(mock,foo)).Return(1).Return(2).Return(3);

        // Return the same value many times (56 in this example)
        When(Method(mock,foo)).Return(56_Times(1));

        // Return many values many times (First 100 calls will return 1, next 200 calls will return 2)
        When(Method(mock,foo)).Return(100_Times(1), 200_Times(2));

        // Always return a value (The next two lines do exactly the same)
        When(Method(mock,foo)).AlwaysReturn(1);
        Method(mock,foo) = 1;
    }


    void testVerifyMethodInvocation()
    {
        Mock<Protocol> mock;

        When(Method(mock,foo)).AlwaysReturn(999); // overwritten, so could be the default stub
        When(Method(mock,foo)).Return(0);

        IProtocol &i = mock.get();

        // Production code
        int expectZero = i.foo(1);
        QCOMPARE(expectZero, 0);

        // Verify method mock.foo was invoked.
        Verify(Method(mock,foo));

        // Verify method mock.foo was invoked with specific arguments.
        Verify(Method(mock,foo).Using(1));
    }

    void testMockThroughQSharedPointer()
    {
        Mock<Protocol> originalMock;

        // move constructor of Mock<Protocol> is called, so objects are not copied
        auto&& theInterface = QSharedPointer<Mock<Protocol>>::create(originalMock.get());

        auto& mock = *theInterface;
        QCOMPARE(&originalMock.get(), &mock.get());

        When(Method(mock,foo)).Return(8);

        int fooReturn = mock.get().foo(1);
        QCOMPARE(fooReturn, 8);
    }

    void testImplementorWithMock()
    {
        // Create mock for Protocol
        Mock<Protocol> originalMock;
        auto mockprotocol = QSharedPointer<Protocol>(&originalMock.get(), [](Protocol *obj){ /*delete handled by mock*/});
        QCOMPARE(&(originalMock.get()), mockprotocol.get());

        // Define stub behaviour
        When(Method(originalMock,foo)).Return(8).Return(88);
        When(Method(originalMock,bar).Using(6, _)).Return(888);
        When(Method(originalMock,bar).Using(_, 66)).Return(8888);

        // Test implementor with protocol
        Implementor implementor(mockprotocol);
        int return1 = implementor.foo(666); // ignore input
        int return2 = implementor.foo(666);  // ignore input
        int return3 = implementor.bar(6, 666);  // only 2nd ignore input
        // implementor.bar(123, 456);  //--> will fail: expect 2nd input parameter to be 66, as in next call
        int return4 = implementor.bar(666, 66);  // only 2nd ignore input

        // Verify result
        QCOMPARE(return1, 8);
        QCOMPARE(return2, 88);
        QCOMPARE(return3, 888);
        QCOMPARE(return4, 8888);
    }

    void testImplementorWithOutputParameter()
    {
        // Create mock for Protocol
        Mock<Protocol> originalMock;
        auto mockprotocol = QSharedPointer<Protocol>(&originalMock.get(), [](Protocol *obj){ /*delete handled by mock*/});
        QCOMPARE(&(originalMock.get()), mockprotocol.get());

        // Define stub behaviour
        auto expectedColors1 = Colors{1,2,3}, expectedColors2 = Colors{4,5,6};
        When(OverloadedMethod(originalMock, getColors, bool(Colors&)))
             .Do([&](Colors& colors)
                 {
                    colors = expectedColors1; // set output parameter
                    return true; // set return value
                 })
             .Do([&](Colors& colors)
                 {
                     colors = expectedColors2;
                     return false;
                 });

        // Test implementor with protocol
        Colors actualColors1, actualColors2;
        Implementor implementor(mockprotocol);
        bool success = implementor.getColors(actualColors1); // ignore input
        QCOMPARE(success, true);

        success = implementor.getColors(actualColors2);
        QCOMPARE(success, false);

        // Verify result
        QCOMPARE(actualColors1, expectedColors1);
        QCOMPARE(actualColors2, expectedColors2);
    }

    void testMockUsingReturnValue()
    {
        // Create mock for Protocol
        Mock<Protocol> originalMock;

        // Define stub behaviour
        auto expectedColors1 = Colors{1,2,3}, expectedColors2 = Colors{4,5,6};
        When(ConstOverloadedMethod(originalMock, getColors, ReturnValue<Colors>())).Return(expectedColors1, expectedColors2);

        // Test implementor with protocol
        Protocol &mock = originalMock.get();
        auto actualColors1 = mock.getColors();
        QVERIFY2(actualColors1.isValid(), qUtf8Printable(actualColors1.errorString()));

        auto actualColors2 = mock.getColors();
        QVERIFY2(actualColors2.isValid(), qUtf8Printable(actualColors1.errorString()));

        // Verify result
        QCOMPARE(actualColors1, expectedColors1);
        QCOMPARE(actualColors2, expectedColors2);
    }
};

QTEST_GUILESS_MAIN(SimpleTest)

#include "main.moc" // to use Test namespace
