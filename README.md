# Proposal: Mocking using fakeit framework

## Intro

Fake-It (eranpeer/FakeIt) is a simple mocking framework for C++. FakeIt uses the latest C++11 features to create an expressive (yet very simple) API. With FakeIt there is no need for re-declaring methods nor creating a derived class for each mock. It is compatible with GCC, Clang and VC+
Furthermore it is extremely handy, concise and very powerful. The biggest reason to use it, in my opinion, would be to easily improve our coverage.

Here is a shorter example/demonstration on how to use FakeIt that lets you specify ANYTHING you want in order to test properly: https://gist.github.com/jaques-sam/590a5301dcd3b3f2fd68de4ab110d6b2

## Usage

Per Qt test case, there is 1 mocking object by specifying:

``` C++
Mock<BarcoDisplayProtocol>
```

Now create a stub with your favorite in/out parameters and return value:
```C++
When(Method(m_mockprotocol, getSelectedDisplayFunction)).AlwaysDo([&](qint32 &index)
 { index = lutIndexInDisplay; return true; });
 
// Actual test which calls m_mockprotocol.getSelectedDisplayFunction()
const auto& getRv = m_displayFunction.selectedDisplayFunction();
QVERIFY(!getRv.isValid());
```
(this one has an output parameter, so a lambda is used to set it):

#### Optional verification

Example error on verifying that the following method is called once, wathever its parameter or return value:
```C++
Verify(Method(m_mockprotocol, getSelectedDisplayFunction)).AtLeastOnce();
```


This could be the error in case you add the `Verify`, but the mock was not called:
```
FAIL!  : DisplayFunctionTest::testDisplayFunctionValid(DynGamma18) Verification error
Expected pattern: m_mockprotocol.setSelectedDisplayFunction( Any arguments )
Expected matches: at least 1
Actual matches  : 0
Actual sequence : total of 1 actual invocations:
  m_mockprotocol.getSelectedDisplayFunction(601)
```

## Integration
As you can see in the CMake file, the qt single header is downloaded from a forked repo where I made 1 change to make it work for the Windows build when building using CMakeSpark (https://github.com/frederikvannoote/cmake-common). This change has already requested to [the original FakeIt repo](https://github.com/eranpeer/FakeIt).

We have to enable RTTI and exceptions in order to use the header. As this it's only used within tests, so we don't see why this would cause any trouble. I succeeded in enabling RTTI by using different CXX flags for the test but still have the following warning: `cl : Command line warning D9025 : overriding '/GR-' with '/GR'`. I guess this has to be solved in the CMake spark repo to define 2 types of CXX flags, but not sure.

For the Windows build, enabling RTTI is enough to get everything build. Of course there are more stricter rules enabled in CMakeSpark for gcc & clang, so these warnings have to be suppressed within CXX flags.

## Conclusion

### Pros & Cons
\+ Coverage can be increased: any possible in/out parameter combination can be tested easily
\+ Stubs can be defined on top of the test function but also in 1 place for the full testcase
\+ Very easy API to use
\+ Easy to integrate because of a single header
\+ Avoids the need of a mocking class reducing large _src_ code structures
\+ Supports features like argument matching, spying, mocking overload etc.

\- Uses RTTI & exceptions + other build warnings, if build using CMake Spark
\- Could become complex in some cases, but depends on how complicated you want to create the test
\- Use of lambdas for output parameters, still easy readable
\- Has a known bug where sometimes a function name is not given if not mocked

See others on the official repo main page.
